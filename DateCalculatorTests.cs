﻿using System;
using Xunit;
using CarInfosAPI.Controllers;

namespace UnitTestProject1
{
    public class CalculateDifferenceTest
    {
        [Fact]
        public void OneSecondsAgo()
        {
            var olderDate = new DateTime(2017, 12, 17, 9, 0, 9);
            var newerDate = new DateTime(2017, 12, 17, 9, 0, 10);
            var diff = TimeCalculator.CalculateTimeDifference(olderDate, newerDate);
            Assert.Equal(diff, "one second ago");
        }

        [Fact]
        public void TenSecondsAgo()
        {
            var olderDate = new DateTime(2017, 12, 17, 9, 0, 0);
            var newerDate = new DateTime(2017, 12, 17, 9, 0, 10);
            var diff = TimeCalculator.CalculateTimeDifference(olderDate, newerDate);
            Assert.Equal(diff, "10 seconds ago");
        }

        [Fact]
        public void OneMinuteAgo()
        {
            var olderDate = new DateTime(2017, 12, 17, 9, 0, 0);
            var newerDate = new DateTime(2017, 12, 17, 9, 1, 10);
            var diff = TimeCalculator.CalculateTimeDifference(olderDate, newerDate);
            Assert.Equal(diff, "a minute ago");
        }

        [Fact]
        public void FifteenMinutesAgo()
        {
            var olderDate = new DateTime(2017, 12, 17, 9, 0, 0);
            var newerDate = new DateTime(2017, 12, 17, 9, 15, 0);
            var diff = TimeCalculator.CalculateTimeDifference(olderDate, newerDate);
            Assert.Equal(diff, "15 minutes ago");
        }

        [Fact]
        public void OneHourAgo()
        {
            var olderDate = new DateTime(2017, 12, 17, 9, 0, 0);
            var newerDate = new DateTime(2017, 12, 17, 10, 1, 10);
            var diff = TimeCalculator.CalculateTimeDifference(olderDate, newerDate);
            Assert.Equal(diff, "an hour ago");
        }

        [Fact]
        public void FiveHoursAgo()
        {
            var olderDate = new DateTime(2017, 12, 17, 9, 0, 0);
            var newerDate = new DateTime(2017, 12, 17, 14, 0, 0);
            var diff = TimeCalculator.CalculateTimeDifference(olderDate, newerDate);
            Assert.Equal(diff, "5 hours ago");
        }

        [Fact]
        public void Yesterday()
        {
            var olderDate = new DateTime(2017, 12, 16, 9, 0, 0);
            var newerDate = new DateTime(2017, 12, 17, 9, 0, 0);
            var diff = TimeCalculator.CalculateTimeDifference(olderDate, newerDate);
            Assert.Equal(diff, "yesterday");
        }

        [Fact]
        public void TwoDaysAgo()
        {
            var olderDate = new DateTime(2017, 12, 15, 9, 0, 0);
            var newerDate = new DateTime(2017, 12, 17, 9, 0, 0);
            var diff = TimeCalculator.CalculateTimeDifference(olderDate, newerDate);
            Assert.Equal(diff, "2 days ago");
        }

        [Fact]
        public void OneMonth()
        {
            var olderDate = new DateTime(2017, 11, 14, 9, 0, 0);
            var newerDate = new DateTime(2017, 12, 25, 9, 0, 0);
            var diff = TimeCalculator.CalculateTimeDifference(olderDate, newerDate);
            Assert.Equal(diff, "one month ago");
        }

        [Fact]
        public void TenMonthsAgo()
        {
            var olderDate = new DateTime(2017, 2, 14, 9, 0, 0);
            var newerDate = new DateTime(2017, 12, 25, 9, 0, 0);
            var diff = TimeCalculator.CalculateTimeDifference(olderDate, newerDate);
            Assert.Equal(diff, "10 months ago");
        }

        [Fact]
        public void OneYearAgo()
        {
            var olderDate = new DateTime(2016, 2, 14, 9, 0, 0);
            var newerDate = new DateTime(2017, 12, 25, 9, 0, 0);
            var diff = TimeCalculator.CalculateTimeDifference(olderDate, newerDate);
            Assert.Equal(diff, "one year ago");
        }

        [Fact]
        public void FiveYearsAgo()
        {
            var olderDate = new DateTime(2012, 10, 14, 9, 0, 0);
            var newerDate = new DateTime(2017, 12, 25, 9, 0, 0);
            var diff = TimeCalculator.CalculateTimeDifference(olderDate, newerDate);
            Assert.Equal(diff, "5 years ago");
        }
    }
}

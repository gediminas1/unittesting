﻿using Xunit;
using CarInfosAPI.Controllers;

namespace XUnitTestProject1
{
    public class EmailFormerTests
    {
        [Fact]
        public void FullEmail()
        {
            string email = EmailFormer.FormEmail("Tom", "2017-12-13");
            string expected = "Dear Tom, thank you for your subscription. ";
            expected += "You have been subscribed to the newsletter until 2017-12-13.";
            Assert.Equal(email, expected);
        }

        [Fact]
        public void IndefiniteDateEmail()
        {
            string email = EmailFormer.FormEmail("Tom", "indefinitely");
            string expected = "Dear Tom, thank you for your subscription. ";
            expected += "You have been subscribed to the newsletter indefinitely.";
            Assert.Equal(email, expected);
        }

        [Fact]
        public void FailedDateEmail()
        {
            string email = EmailFormer.FormEmail("", "");
            string expected = "Email forming failed.";
            Assert.Equal(email, expected);
        }
    }
}

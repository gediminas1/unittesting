using CarInfosAPI.Controllers;
using CarInfosAPI.Entities;
using CarInfosAPI.Models.News;
using CarInfosAPI.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using Xunit;
using AutoMapper;
using Xunit.Abstractions;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using CarInfosAPI.Models.User;
using CarInfosAPI.Models.Review;
using CarInfosAPI.Models.Comment;

namespace XUnitTestProject1
{
    public class UnitTest1
    {
        private readonly ITestOutputHelper output;
        private ICarInfoRepository carRepository;
        private NewsController newsController;
        private ReviewsController reviewController;
        private CommentsController commentController;
        private UsersCotroller userController;
        private User user = new User();
        private User user2 = new User();
        private News news1 = new News();
        private News news2 = new News();
        private Review review1 = new Review();
        private Review review2 = new Review();
        private Comment comment1 = new Comment();
        private Comment comment2 = new Comment();

        public UnitTest1(ITestOutputHelper output)
        {
            this.output = output;
            AutoMapper.Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<Review, ReviewModel>();
                cfg.CreateMap<ReviewForCreationModel, Review>();
                cfg.CreateMap<ReviewForUpdateModel, Review>();
                cfg.CreateMap<ReviewModel, Review>();
                cfg.CreateMap<ReviewModel, ReviewForCreationModel>();
                cfg.CreateMap<ReviewModel, ReviewForUpdateModel>();

                cfg.CreateMap<News, NewsModel>();
                cfg.CreateMap<NewsForCreationModel, News>();
                cfg.CreateMap<NewsForUpdateModel, News>();
                cfg.CreateMap<NewsModel, News>();
                cfg.CreateMap<NewsModel, NewsForCreationModel>();
                cfg.CreateMap<NewsModel, NewsForUpdateModel>();

                cfg.CreateMap<User, UserModel>();
                cfg.CreateMap<UserForCreationModel, User>();
                cfg.CreateMap<UserForUpdateModel, User>();
                cfg.CreateMap<UserModel, User>();
                cfg.CreateMap<UserModel, UserForCreationModel>();
                cfg.CreateMap<UserModel, UserForUpdateModel>();

                cfg.CreateMap<Comment, CommentModel>();
                cfg.CreateMap<CommentForCreationModel, Comment>();
                cfg.CreateMap<CommentForUpdateModel, Comment>();
                cfg.CreateMap<CommentModel, Comment>();
                cfg.CreateMap<CommentModel, CommentForCreationModel>();
                cfg.CreateMap<CommentModel, CommentForUpdateModel>();
            });
            var connectionString = "Server=(localdb)\\mssqllocaldb;Database=CarInfoDBTesting;Trusted_Connection=True;";
            var optionsBuilder = new DbContextOptionsBuilder<CarInfoContext>();
            optionsBuilder.UseSqlServer(connectionString);
            carRepository = new CarInfoRepository(new CarInfoContext(optionsBuilder.Options));
            carRepository.ClearDB();

            newsController = new NewsController(carRepository, new HttpContextAccessor());
            reviewController = new ReviewsController(carRepository, new HttpContextAccessor());
            commentController = new CommentsController(carRepository, new HttpContextAccessor());
            userController = new UsersCotroller(carRepository, new HttpContextAccessor());

            user.Username = "testas";
            user.Email = "testas@gmail.com";
            user.Password = "37c991c90a014af3ddcf52db8d92ce846b74eb040c8242cb207a3d3666cc6b87";
            user.Name = "testas";
            user.Surname = "testas";
            carRepository.AddUser(user);
            carRepository.Save();

            user2.Username = "testas2";
            user2.Email = "testas2@gmail.com";
            user2.Password = "testas2";
            user2.Name = "testas2";
            user2.Surname = "testas2";
            carRepository.AddUser(user2);
            carRepository.Save();

            news1.Date = DateTime.Now;
            news1.ImageUrl = "image_1";
            news1.Description = "description_1";
            carRepository.AddNews(news1, 1);
            carRepository.Save();

            news2.Date = DateTime.Now;
            news2.ImageUrl = "image_2";
            news2.Description = "description_2";
            carRepository.AddNews(news2, 1);
            carRepository.Save();

            review1.Date = DateTime.Now;
            review1.ImageUrl = "review_image_1";
            review1.Description = "review_description_1";
            carRepository.AddReview(review1, 1);
            carRepository.Save();

            review2.Date = DateTime.Now;
            review2.ImageUrl = "review_image_2";
            review2.Description = "review_description_2";
            carRepository.AddReview(review2, 1);
            carRepository.Save();

            comment1.Date = DateTime.Now;
            comment1.Description = "comment_1";
            carRepository.AddComment(1, 1, comment1);
            carRepository.Save();

            comment2.Date = DateTime.Now;
            comment2.Description = "comment_2";
            carRepository.AddComment(1, 1, comment2);
            carRepository.Save();
        }

        [Fact]
        public void TestGetAllNews()
        { 
            var result = newsController.GetNews();
            var okObjectResult = result as OkObjectResult;
            Assert.NotNull(okObjectResult);
            var model = okObjectResult.Value as IEnumerable<NewsModel>;           
            Assert.NotNull(model);
            var items = model.GetEnumerator();
            items.MoveNext();
            var newsToCompare = Mapper.Map<News>(items.Current);
            Assert.Equal(news1.Description, newsToCompare.Description);
            items.MoveNext();
            newsToCompare = Mapper.Map<News>(items.Current);
            Assert.Equal(news2.Description, newsToCompare.Description);

        }

        [Fact]
        public void TestGetNewsByID()
        {
            var result = newsController.GetNews(1);
            var okObjectResult = result as OkObjectResult;
            Assert.NotNull(okObjectResult);
            var model = okObjectResult.Value as NewsModel;
            Assert.NotNull(model);
            var newsToCompare = Mapper.Map<News>(model);
            Assert.Equal(news1.Description, newsToCompare.Description);

            result = newsController.GetNews(5);
            var notFound = result as NotFoundResult;
            Assert.NotNull(notFound);

        }

        [Fact]
        public void TestCreateNews()
        {
            var news = new NewsForCreationModel();
            news.Date = DateTime.Now;
            news.ImageUrl = "image_3";
            news.Description = "description_3";

            var result = newsController.CreateNews(news, 1);
            var createdAtRouteAction = result as CreatedAtRouteResult;
            Assert.NotNull(createdAtRouteAction);

            result = newsController.GetNews(3);
            var okObjectResult = result as OkObjectResult;
            Assert.NotNull(okObjectResult);
            var model = okObjectResult.Value as NewsModel;
            Assert.NotNull(model);
            var newsToCompare = Mapper.Map<NewsForCreationModel>(model);
            Assert.Equal(news.Description, newsToCompare.Description);

        }

        [Fact]
        public void TestUpdateNews()
        {
            var news = new NewsForUpdateModel();
            news.Date = DateTime.Now;
            news.ImageUrl = "image_2";
            news.Description = "description_2_updated";

            var result = newsController.UpdateNews(2, news);
            var noContentResult = result as NoContentResult;
            Assert.NotNull(noContentResult);

            result = newsController.GetNews(2);
            var okObjectResult = result as OkObjectResult;
            Assert.NotNull(okObjectResult);
            var model = okObjectResult.Value as NewsModel;
            Assert.NotNull(model);
            var newsToCompare = Mapper.Map<NewsForUpdateModel>(model);
            Assert.Equal(news.Description, newsToCompare.Description);

            result = newsController.UpdateNews(5, news);
            var notFound = result as NotFoundResult;
            Assert.NotNull(notFound);
        }

        [Fact]
        public void TestDeleteNews()
        {
            var result = newsController.DeleteNews(2);
            var noContentResult = result as NoContentResult;
            Assert.NotNull(noContentResult);

            result = newsController.DeleteNews(5);
            var notFound = result as NotFoundResult;
            Assert.NotNull(notFound);
        }

        [Fact]
        public void TestGetAllReviews()
        {
            var result = reviewController.GetReviews();
            var okObjectResult = result as OkObjectResult;
            Assert.NotNull(okObjectResult);
            var model = okObjectResult.Value as IEnumerable<ReviewModel>;
            Assert.NotNull(model);
            var items = model.GetEnumerator();
            items.MoveNext();
            var reviewToCompare = Mapper.Map<Review>(items.Current);
            Assert.Equal(review1.Description, reviewToCompare.Description);
            items.MoveNext();
            reviewToCompare = Mapper.Map<Review>(items.Current);
            Assert.Equal(review2.Description, reviewToCompare.Description);
        }

        [Fact]
        public void TestGetReviewByID()
        {
            var result = reviewController.GetReview(1);
            var okObjectResult = result as OkObjectResult;
            Assert.NotNull(okObjectResult);
            var model = okObjectResult.Value as ReviewModel;
            Assert.NotNull(model);
            var reviewToCompare = Mapper.Map<Review>(model);
            Assert.Equal(review1.Description, reviewToCompare.Description);

            result = reviewController.GetReview(5);
            var notFound = result as NotFoundResult;
            Assert.NotNull(notFound);

        }

        [Fact]
        public void TestCreateReview()
        {
            var review = new ReviewForCreationModel();
            review.ImageUrl = "review_image_3";
            review.Description = "review_description_3";

            var result = reviewController.CreateReview(review, 1);
            var createdAtRouteAction = result as CreatedAtRouteResult;
            Assert.NotNull(createdAtRouteAction);

            result = reviewController.GetReview(3);
            var okObjectResult = result as OkObjectResult;
            Assert.NotNull(okObjectResult);
            var model = okObjectResult.Value as ReviewModel;
            Assert.NotNull(model);
            var newsToCompare = Mapper.Map<ReviewForCreationModel>(model);
            Assert.Equal(review.Description, newsToCompare.Description);

        }

        [Fact]
        public void TestUpdateReview()
        {
            var review = new ReviewForUpdateModel();
            review.ImageUrl = "review_image_2";
            review.Description = "review_description_2_updated";

            var result = reviewController.UpdateReview(2, review);
            var noContentResult = result as NoContentResult;
            Assert.NotNull(noContentResult);

            result = reviewController.GetReview(2);
            var okObjectResult = result as OkObjectResult;
            Assert.NotNull(okObjectResult);
            var model = okObjectResult.Value as ReviewModel;
            Assert.NotNull(model);
            var reviewToCompare = Mapper.Map<ReviewForUpdateModel>(model);
            Assert.Equal(review.Description, reviewToCompare.Description);

            result = reviewController.UpdateReview(5, review);
            var notFound = result as NotFoundResult;
            Assert.NotNull(notFound);
        }

        [Fact]
        public void TestDeleteReview()
        {
            var result = reviewController.DeleteReview(2);
            var noContentResult = result as NoContentResult;
            Assert.NotNull(noContentResult);

            result = reviewController.DeleteReview(5);
            var notFound = result as NotFoundResult;
            Assert.NotNull(notFound);
        }

        [Fact]
        public void TestGetAllComments()
        {
            var result = commentController.GetComments(5);
            var badRequestObjectResult = result as BadRequestObjectResult;
            Assert.NotNull(badRequestObjectResult);

            result = commentController.GetComments(1);
            var okObjectResult = result as OkObjectResult;
            Assert.NotNull(okObjectResult);
            var model = okObjectResult.Value as IEnumerable<CommentModel>;
            Assert.NotNull(model);
            var items = model.GetEnumerator();
            items.MoveNext();
            var commentToCompare = Mapper.Map<Comment>(items.Current);
            Assert.Equal(comment2.Description, commentToCompare.Description);
            items.MoveNext();
            commentToCompare = Mapper.Map<Comment>(items.Current);
            Assert.Equal(comment1.Description, commentToCompare.Description);
        }

        [Fact]
        public void TestGetCommentByID()
        {
            var result = commentController.GetComment(5, 1);
            var badRequestObjectResult = result as BadRequestObjectResult;
            Assert.NotNull(badRequestObjectResult);

            result = commentController.GetComment(1, 1);
            var okObjectResult = result as OkObjectResult;
            Assert.NotNull(okObjectResult);
            var model = okObjectResult.Value as CommentModel;
            Assert.NotNull(model);
            var commentToCompare = Mapper.Map<Comment>(model);
            Assert.Equal(comment1.Description, commentToCompare.Description);

            result = commentController.GetComment(1, 5);
            var notFound = result as NotFoundResult;
            Assert.NotNull(notFound);

        }

        [Fact]
        public void TestCreateComment()
        {
            var comment = new CommentForCreationModel();
            comment.Description = "comment_3";

            var result = commentController.CreateComment(5, comment, 1);
            var badRequestObjectResult = result as BadRequestObjectResult;
            Assert.NotNull(badRequestObjectResult);

            result = commentController.CreateComment(1, comment, 1);
            var createdAtRouteAction = result as CreatedAtRouteResult;
            Assert.NotNull(createdAtRouteAction);

            result = commentController.GetComment(1, 3);
            var okObjectResult = result as OkObjectResult;
            Assert.NotNull(okObjectResult);
            var model = okObjectResult.Value as CommentModel;
            Assert.NotNull(model);
            var commentToCompare = Mapper.Map<CommentForCreationModel>(model);
            Assert.Equal(comment.Description, commentToCompare.Description);

        }

        [Fact]
        public void TestUpdateComment()
        {
            var comment = new CommentForUpdateModel();
            comment.Description = "comment_2_updated";

            var result = commentController.UpdateComment(5, 2, comment);
            var badRequestObjectResult = result as BadRequestObjectResult;
            Assert.NotNull(badRequestObjectResult);

            result = commentController.UpdateComment(1, 2, comment);
            var noContentResult = result as NoContentResult;
            Assert.NotNull(noContentResult);

            result = commentController.GetComment(1, 2);
            var okObjectResult = result as OkObjectResult;
            Assert.NotNull(okObjectResult);
            var model = okObjectResult.Value as CommentModel;
            Assert.NotNull(model);
            var commentToCompare = Mapper.Map<CommentForUpdateModel>(model);
            Assert.Equal(comment.Description, commentToCompare.Description);
        }

        [Fact]
        public void TestDeleteComment()
        {
            var result = commentController.DeleteComment(1, 2);
            var noContentResult = result as NoContentResult;
            Assert.NotNull(noContentResult);

            result = commentController.DeleteComment(1, 5);
            var notFound = result as NotFoundResult;
            Assert.NotNull(notFound);
        }

        [Fact]
        public void TestGetAllUsers()
        {
            var result = userController.GetUsers();
            var okObjectResult = result as OkObjectResult;
            Assert.NotNull(okObjectResult);
            var model = okObjectResult.Value as IEnumerable<UserModel>;
            Assert.NotNull(model);
            var items = model.GetEnumerator();
            items.MoveNext();
            var userToCompare = Mapper.Map<User>(items.Current);
            Assert.Equal(user.Username, userToCompare.Username);
            items.MoveNext();
            userToCompare = Mapper.Map<User>(items.Current);
            Assert.Equal(user2.Username, userToCompare.Username);

        }

        [Fact]
        public void TestGetUserByID()
        {
            var result = userController.GetUser(1);
            var okObjectResult = result as OkObjectResult;
            Assert.NotNull(okObjectResult);
            var model = okObjectResult.Value as UserModel;
            Assert.NotNull(model);
            var userToCompare = Mapper.Map<User>(model);
            Assert.Equal(user.Username, userToCompare.Username);

            result = userController.GetUser(5);
            var notFound = result as NotFoundResult;
            Assert.NotNull(notFound);

        }

        [Fact]
        public void TestUserRegister()
        {
            var user = new UserForCreationModel();
            user.Username = "testas";
            user.Email = "testas3@gmail.com";
            user.Password = "testas3";
            user.Name = "testas3";
            user.Surname = "testas3";

            var result = userController.Register(user);
            var badRequestObjectResult = result as BadRequestObjectResult;
            Assert.NotNull(badRequestObjectResult);

            user.Username = "testas3";
            result = userController.Register(user);
            var createdAtRouteAction = result as CreatedAtRouteResult;
            Assert.NotNull(createdAtRouteAction);

            result = userController.GetUser(3);
            var okObjectResult = result as OkObjectResult;
            Assert.NotNull(okObjectResult);
            var model = okObjectResult.Value as UserModel;
            Assert.NotNull(model);
            var userToCompare = Mapper.Map<UserForCreationModel>(model);
            Assert.Equal(user.Username, userToCompare.Username);

        }

        [Fact]
        public void TestUpdateUser()
        {
            var user = new UserForUpdateModel();
            user.Email = "testas2@gmail.com";
            user.Password = "testas2";
            user.Name = "testas2_updated";
            user.Surname = "testas2";

            var result = userController.UpdateUser(2, user);
            var noContentResult = result as NoContentResult;
            Assert.NotNull(noContentResult);

            result = userController.GetUser(2);
            var okObjectResult = result as OkObjectResult;
            Assert.NotNull(okObjectResult);
            var model = okObjectResult.Value as UserModel;
            Assert.NotNull(model);
            var userToCompare = Mapper.Map<UserForUpdateModel>(model);
            Assert.Equal(user.Name, userToCompare.Name);

            result = userController.UpdateUser(5, user);
            var notFound = result as NotFoundResult;
            Assert.NotNull(notFound);
        }

        [Fact]
        public void TestDeleteUser()
        {
            var result = userController.DeleteUser(2);
            var noContentResult = result as NoContentResult;
            Assert.NotNull(noContentResult);

            result = userController.DeleteUser(5);
            var notFound = result as NotFoundResult;
            Assert.NotNull(notFound);
        }

        [Fact]
        public void TestUserLogin()
        {
            var user = new UserForSignInModel();
            user.Username = "testas";
            user.Password = "testas";

            var result = userController.Login(user);
            var okObjectResult = result as OkObjectResult;
            Assert.NotNull(okObjectResult);

            user.Username = "reksas";
            result = userController.Login(user);
            var badRequestObjectResult = result as BadRequestObjectResult;
            Assert.NotNull(badRequestObjectResult);
        }
    }
}
